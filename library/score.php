<?php
/**
 * Hang-A-Friend (http://hangafriend.com/)
 *
 * @copyright Copyright (c) 2014 Teej (http://www.teej.ca)
 * @link      http://hangafriend.com
 * @license   http://hangafriend.com/LICENSE.txt
 */
namespace Hangman;

class Score
{
    /**
     * @var bool
     */
    private $won = false;

    /**
     * @var int
     */
    private $score = 0;

    /**
     * @var int
     */
    private $created;

    public function __construct()
    {
        $this->created = time();
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return bool
     */
    public function getWon()
    {
        return $this->won;
    }

    /**
     * @param bool $won
     */
    public function setWon($won)
    {
        $this->won = $won;
    }
}
