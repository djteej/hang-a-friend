<?php
/**
 * Hang-A-Friend (http://hangafriend.com/)
 *
 * @copyright Copyright (c) 2014 Teej (http://www.teej.ca)
 * @link      http://hangafriend.com
 * @license   http://hangafriend.com/LICENSE.txt
 */
namespace Hangman;

class Twitter
{
    /**
     * @var string
     */
    private $protocol;

    /**
     * @var string
     */
    private $appURL;

    public function __construct($protocol = 'http')
    {
        $this->protocol = $protocol;
        $this->appURL = $this->protocol .'://' . $_SERVER['HTTP_HOST'] . '/';
    }

    /**
     * @param string $id
     * @return string
     */
    public function getTweetURL($id, $word)
    {
        $puzzle = '&nbsp;';

        foreach (str_split($word) as $letter) {

            if (ctype_alpha($letter)) {

                $puzzle.= '_&nbsp;';

            } else {

                $puzzle.= ' &nbsp;';
            }
        }

        $text = 'Can YOU solve this hangman puzzle?';
        $text = implode(urlencode(PHP_EOL), array($text, '', $puzzle, '', ''));

        return 'https://twitter.com/share?url=' . $this->appURL . '?id=' . $id . '&counturl=' . $this->appURL . '?id=' . $id . '&text=' . $text . '&via=hangafriend';
    }
}
