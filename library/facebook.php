<?php
/**
 * Hang-A-Friend (http://hangafriend.com/)
 *
 * @copyright Copyright (c) 2014 Teej (http://www.teej.ca)
 * @link      http://hangafriend.com
 * @license   http://hangafriend.com/LICENSE.txt
 */
namespace Hangman;

use \Exception;

class Facebook
{
    /**
     * @var string
     */
    private $appURL;

    /**
     * @var string
     */
    private $loginURL;

    /**
     * @var string
     */
    private $ogURL;

    /**
     * @var string
     */
    private $tokenURL;

    /**
     * @var string
     */
    private $profileURL;

    /**
     * @var string
     */
    private $protocol;

    /**
     * @var array
     */
    private $config;

    public function __construct(array $config, $protocol = 'http')
    {
        if (empty($config['id']) || empty($config['secret'])) throw new Exception('Invalid config data');

        $this->config = $config;
        $this->protocol = $protocol;
        $this->ogURL = 'https://graph.facebook.com';
        $this->appURL = $this->protocol .'://' . $_SERVER['HTTP_HOST'] . '/';
        $this->loginURL = $this->protocol . '://www.facebook.com/dialog/oauth?client_id=' . $this->config['id'] . '&redirect_uri=' . $this->appURL . '&scope=email';
    }

    public function connectUser(User $user)
    {
        //Access Token is here
        $accessToken = file_get_contents($this->tokenURL);

        //Access token can give you the details of current user
        $data = json_decode(file_get_contents($this->ogURL . '/me?' . $accessToken));

        if (!empty($data)) {
            $user->setId(($data->id) ? $data->id : null);
            $user->setName(($data->first_name) ? $data->first_name : null);
            $user->setEmail(($data->email) ? $data->email : null);
            $user->setGender(($data->gender) ? $data->gender : null);
            $user->setLink(($data->link) ? $data->link : null);
            $user->setLocale(($data->locale) ? $data->locale : null);
            $user->setCreated();
            $user->setUpdated();

            if ($user->getId() > 0) {

                $user->setConnected(true);

                return true;
            }
        }
        return false;
    }

    /**
     * @param string $id
     * @return string
     */
    public function getShareURL($id)
    {
        return 'https://www.facebook.com/dialog/share?app_id=' . $this->config['id'] . '&display=popup&href=' . $this->appURL . '?id=' . $id . '&redirect_uri=' . $this->appURL;
    }

    /**
     * @param string $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return string
     */
    public function getLoginURL()
    {
        return $this->loginURL;
    }

    /**
     * @return string
     */
    public function getProfileURL()
    {
        return $this->profileURL;
    }

    /**
     * @param string $profileURL
     */
    public function setProfileURL($profileURL)
    {
        $this->profileURL = $profileURL;
    }

    /**
     * @return string
     */
    public function getTokenURL()
    {
        return $this->tokenURL;
    }

    /**
     * @param string $code
     */
    public function setTokenURL($code)
    {
        $this->tokenURL = $this->ogURL . '/oauth/access_token?client_id=' . $this->config['id'] . '&redirect_uri=' . $this->appURL . '&client_secret=' . $this->config['secret'] . '&code=' . $code . '&scope=email';
    }
}
