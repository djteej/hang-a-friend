<?php
/**
 * Hang-A-Friend (http://hangafriend.com/)
 *
 * @copyright Copyright (c) 2014 Teej (http://www.teej.ca)
 * @link      http://hangafriend.com
 * @license   http://hangafriend.com/LICENSE.txt
 */
namespace Hangman;

class User
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var int
     */
    private $created;

    /**
     * @var int
     */
    private $updated;

    /**
     * @var bool
     */
    private $connected = false;

    public function isConnected()
    {
        return $this->connected;
    }

    /**
     * @param bool $connected
     */
    public function setConnected($connected)
    {
        $this->connected = $connected;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    public function setCreated()
    {
        $this->created = time();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    public function setUpdated()
    {
        $this->updated = time();
    }
}
