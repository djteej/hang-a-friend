<?php
/**
 * Hang-A-Friend (http://hangafriend.com/)
 *
 * @copyright Copyright (c) 2014 Teej (http://www.teej.ca)
 * @link      http://hangafriend.com
 * @license   http://hangafriend.com/LICENSE.txt
 */
namespace Hangman;

use SQLite3;

class Game
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Score
     */
    private $score;

    /**
     * @var bool
     */
    private $over = false;

    /**
     * @var bool
     */
    private $error = false;

    /**
     * @var int
     */
    private $guesses = 5;

    /**
     * @var string
     */
    private $word;

    /**
     * @var string
     */
    private $hint;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $correctLetters = array();

    /**
     * @var array
     */
    private $incorrectLetters = array();

    /**
     * @var array
     */
    private $wordLetters = array();

    /**
     * @var string
     */
    private $created;

    /**
     * @var array
     */
    private $alphabet = array(
        "a", "b", "c", "d", "e", "f", "g", "h",
        "i", "j", "k", "l", "m", "n", "o", "p",
        "q", "r", "s", "t", "u", "v", "w", "x",
        "y", "z");

    public function __destruct()
    {
        $this->message = null;
    }

    public function playGame($request = array())
    {
        // check if game has expired
        if ((time() - $this->created) > (60 * 60 * 8)) $this->resetGame();

        //player is trying to guess a letter
        if (!empty($request['letter'])) $this->guessLetter($request['letter']);

        //check if they've found all the letters in this word
        if (!$this->error && !empty($this->word)) {

            if (array_intersect($this->wordLetters, $this->correctLetters) === array_filter($this->wordLetters, array($this, 'isLetter'))) {

                $this->over = true;
                $this->getScore()->setWon(true);

                $this->message = $this->successMsg('<h2 class="text-center">Woo-hoo! You solved it!</h2>');

            } else if ($this->guesses <= 0) {

                $this->over = true;
                $this->getScore()->setWon(false);

                $this->message = $this->errorMsg('<h2 class="text-center">Dang! Game Over! You ran out of guesses...</h2>');
            }
        }
    }

    public function resetGame()
    {
        $this->getScore()->setScore(0);
        $this->over = false;
        $this->guesses = 5;
        $this->correctLetters = array();
        $this->incorrectLetters = array();
        $this->wordLetters = str_split(strtolower($this->word));
        $this->created = time();

    }

    public function createGame(SQLite3 $db, $request = null)
    {
        $this->id = uniqid();
        $this->word = $this->cleanInput($request['word']);
        $this->hint = $this->cleanInput($request['hint']);
        $this->resetGame();

        $statement = $db->prepare('INSERT INTO games (id, user_id, word, hint, created) VALUES (:id, :user_id, :word, :hint, :created)');
        $statement->bindParam(':id', $this->id, SQLITE3_TEXT);
        $statement->bindParam(':user_id', $this->user->getId(), SQLITE3_TEXT);
        $statement->bindParam(':word', $this->word, SQLITE3_TEXT);
        $statement->bindParam(':hint', $this->hint, SQLITE3_TEXT);
        $statement->bindParam(':created', $this->created, SQLITE3_INTEGER);

        $result = $statement->execute();

        if (!$result) {

            $this->error = true;
            $this->message = $this->errorMsg('Sorry! There was an error creating your game.');

        } else {

            $this->error = false;
        }
    }

    public function userGame(SQLite3 $db, $id)
    {
        $statement = $db->prepare('SELECT * FROM games WHERE id = :id');
        $statement->bindParam(':id', $id, SQLITE3_TEXT);

        $result = $statement->execute();

        if (!$result || !$data = $result->fetchArray(SQLITE3_ASSOC)) {

            $this->error = true;
            $this->message = $this->errorMsg('Sorry! There was an error loading your game.');

        } else {

            $this->error = false;
            $this->id = $data['id'];
            $this->word = $data['word'];
            $this->hint = $data['hint'];
            $this->resetGame();
        }
    }

    public function randomGame(SQLite3 $db)
    {
        $statement = $db->prepare('SELECT * FROM games WHERE user_id IS NULL ORDER BY RANDOM() LIMIT 1');

        $result = $statement->execute();

        if (!$result || !$data = $result->fetchArray(SQLITE3_ASSOC)) {

            $this->error = true;
            $this->message = $this->errorMsg('Sorry! There was an error loading a game.');

        } else {

            $this->error = false;
            $this->id = $data['id'];
            $this->word = $data['word'];
            $this->hint = $data['hint'];
            $this->resetGame();
        }
    }

    public function isError()
    {
        return $this->error;
    }

    public function isOver()
    {
        if ($this->over === true) return true;

        else return false;
    }

    public function saveScore(SQLite3 $db)
    {
        $statement = $db->prepare('INSERT INTO scores (game_id, user_id, won, score, created) VALUES (:game_id, :user_id, :won, :score, :created)');
        $statement->bindParam(':game_id', $this->id, SQLITE3_TEXT);
        $statement->bindParam(':user_id', $this->user->getId(), SQLITE3_TEXT);
        $statement->bindParam(':won', $this->score->getWon(), SQLITE3_INTEGER);
        $statement->bindParam(':score', $this->score->getScore(), SQLITE3_INTEGER);
        $statement->bindParam(':created', time(), SQLITE3_INTEGER);

        $result = $statement->execute();

        if (!$result) {

            $this->message = $this->errorMsg('Sorry! There was an error saving your score.');
        }
    }

    public function saveUser(SQLite3 $db)
    {
        $statement = $db->prepare('INSERT OR IGNORE INTO users (id, created) VALUES (:id, :created)');
        $statement->bindParam(':id', $this->user->getId(), SQLITE3_TEXT);
        $statement->bindParam(':created', time(), SQLITE3_INTEGER);

        $result = $statement->execute();

        if (!$result) {

            $this->message = $this->errorMsg('Sorry! There was an error saving your score.');

        } else {

            $statement = $db->prepare('UPDATE users SET name = :name, email = :email, link = :link, gender = :gender, locale = :locale, updated = :updated WHERE id = :id');
            $statement->bindParam(':id', $this->user->getId(), SQLITE3_TEXT);
            $statement->bindParam(':name', $this->user->getName(), SQLITE3_TEXT);
            $statement->bindParam(':email', $this->user->getEmail(), SQLITE3_TEXT);
            $statement->bindParam(':link', $this->user->getLink(), SQLITE3_TEXT);
            $statement->bindParam(':gender', $this->user->getGender(), SQLITE3_TEXT);
            $statement->bindParam(':locale', $this->user->getLocale(), SQLITE3_TEXT);
            $statement->bindParam(':updated', time(), SQLITE3_INTEGER);

            $result = $statement->execute();

            if (!$result) {

                $this->message = $this->errorMsg('Sorry! There was an error saving your score.');
            }
        }
    }

    private function guessLetter($letter)
    {
        $letter = strtolower($letter);

        if (!$this->isLetter($letter)) {

            $this->message = $this->errorMsg("Oops! Please enter a letter of the alphabet.");

        } elseif (in_array($letter, $this->correctLetters)) {

            $this->message = $this->errorMsg("Oops! You've already guessed this letter.");

        } elseif ($count = count(array_keys($this->wordLetters, $letter, true))) {

            array_push($this->correctLetters, $letter);

            $this->getScore()->setScore($this->getScore()->getScore() + ($count * $this->guesses * 10));

            $this->message = $this->successMsg("Good job! There are $count letter $letter's in this word");

        } else {

            array_push($this->incorrectLetters, $letter);

            $this->guesses -= 1;

            $this->message = $this->errorMsg("Sorry! There are no letter $letter's in this word.");
        }
    }

    private function isLetter($value)
    {
        if (!ctype_alpha($value) || strlen($value) != 1) return false;
        return in_array($value, $this->alphabet);
    }

    /**
     * @return int
     */
    public function getGuesses()
    {
        return $this->guesses;
    }

    public function getHint()
    {
        return $this->hint;
    }

    public function getWord()
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    public function getKeyboard()
    {
        $key = '';

        foreach ($this->alphabet as $letter) {

            if (in_array($letter, $this->correctLetters)) {

                $key.= '<input type="submit" name="letter" value="' . strtoupper($letter) . '" class="btn btn-success" disabled="disabled" /> ';

            } elseif (in_array($letter, $this->incorrectLetters)) {

                $key.= '<input type="submit" name="letter" value="' . strtoupper($letter) . '" class="btn btn-danger" disabled="disabled" /> ';

            } elseif ($this->isOver()) {

                $key.= '<input type="submit" name="letter" value="' . strtoupper($letter) . '" class="btn btn-default" disabled="disabled" /> ';

            } else {

                $key.= '<input type="submit" name="letter" value="' . strtoupper($letter) . '" class="btn btn-default" /> ';
            }
        }
        return $key;
    }

    public function getLeaderboard(SQLite3 $db)
    {
        $leaderboard = '';

        $sql = 'SELECT users.name as name, scores.score as score FROM scores
                LEFT JOIN users on users.id = scores.user_id
                WHERE scores.game_id = :game_id AND scores.won = 1
                GROUP BY scores.user_id
                ORDER BY scores.score DESC, scores.created ASC
                LIMIT 10';

        $statement = $db->prepare($sql);
        $statement->bindParam(':game_id', $this->id, SQLITE3_TEXT);

        $result = $statement->execute();

        if (!$result) {

            $this->message = $this->errorMsg('Sorry! There was an error loading the leaderboard.');

        } else {

            while ($data = $result->fetchArray(SQLITE3_ASSOC)) {

                $name = (!empty($data['name'])) ? $data['name'] : 'Guest';
                $score = (!empty($data['score'])) ? $data['score'] : 0;
                $leaderboard.= '<h4><span class="text-muted">' . $name . '</span><span class="pull-right">'. $score .'</span></h4>';
            }
        }

        if (empty($leaderboard)) {

            $leaderboard.= '<h4 class="text-muted">No one! This must be a tough one, or did you forget to share it with your friends?</h4>';
        }
        return $leaderboard;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @param string $type
     */
    public function setMessage($message, $type = 'success')
    {
        if ($type === 'error') {

            $this->message = $this->errorMsg($message);

        } else {

            $this->message = $this->successMsg($message);
        }
    }

    /**
     * @param bool $hide
     *
     * @return string
     */
    public function getPuzzle($hide = false)
    {
        $result = '&nbsp;';

        foreach (str_split($this->word) as $letter) {

            if (!in_array(strtolower($letter), $this->correctLetters) && $this->isLetter(strtolower($letter))) {

                $result .= '_&nbsp;';

            } elseif ($hide === true && $this->isLetter(strtolower($letter))) {

                $result .= '_&nbsp;';

            } else {

                $result.= $letter . '&nbsp;';
            }
        }
        return $result;
    }

    /**
     * @return Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param Score $score
     */
    public function setScore(Score $score)
    {
        $this->score = $score;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    private function cleanInput($string)
    {
        $string = preg_replace('!\s+!', ' ', $string);
        $string = (strlen($string) > 255) ? substr($string, 0, 255) : $string;

        return $string;
    }

    private function errorMsg($message)
    {
        return '<div class="alert alert-danger" role="alert">' . $message . '</div>';
    }

    private function successMsg($message)
    {
        return '<div class="alert alert-success" role="alert">' . $message . '</div>';
    }
}
