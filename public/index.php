<?php
/**
 * Hang-A-Friend (http://hangafriend.com/)
 *
 * @copyright Copyright (c) 2014 Teej (http://www.teej.ca)
 * @link      http://hangafriend.com
 * @license   http://hangafriend.com/LICENSE.txt
 */
namespace Hangman;

use SQLite3;
use Mailin;

date_default_timezone_set(timezone_name_from_abbr("EST"));

$path = realpath(dirname(__DIR__));

require_once $path . '/library/game.php';
require_once $path . '/library/facebook.php';
require_once $path . '/library/twitter.php';
require_once $path . '/library/score.php';
require_once $path . '/library/user.php';
require_once $path . '/vendor/mailin.php';

$config = require_once $path . '/config/global.php';

$db = new SQLite3($path . '/data/db.sqlite');

session_start();

if (!empty($_SERVER['HTTP_X_FORWARDED_PORT'])) {

    $protocol = ($_SERVER['HTTP_X_FORWARDED_PORT'] == 443) ? 'https' : 'http';

} elseif (!empty($_SERVER['SERVER_PORT'])) {

    $protocol = ($_SERVER['SERVER_PORT'] == 443) ? 'https' : 'http';

} else {

    $protocol = 'http';
}

if (!empty($_REQUEST['logout'])) {

    unset($_SESSION);
    session_destroy();
}

if (!isset($_SESSION['game'])) {

    $_SESSION['game'] = new Game();
    $_SESSION['game']->setUser(new User());
    $_SESSION['game']->setScore(new Score());
    $_SESSION['game']->randomGame($db);
}

if (!isset($_SESSION['facebook'])) $_SESSION['facebook'] = new Facebook($config['facebook'], $protocol);
if (!isset($_SESSION['twitter'])) $_SESSION['twitter'] = new Twitter($protocol);

if (!$_SESSION['game']->getUser()->isConnected()) {

    if (!empty($config['debug'])) {

        $debugUser = new User();
        $debugUser->setId(10153283557829119);
        $debugUser->setEmail('no-reply@teej.ca');
        $debugUser->setName('Trevor');
        $debugUser->setLink('https://www.facebook.com/app_scoped_user_id/10153283557829119/');
        $debugUser->setConnected(true);

        $_SESSION['game']->setUser($debugUser);

    } elseif (!empty($_REQUEST['code'])) {

        $_SESSION['facebook']->setTokenURL($_REQUEST['code']);

        if ($_SESSION['facebook']->connectUser($_SESSION['game']->getUser())) {

            $_SESSION['game']->saveUser($db);

            if (!empty($config['mailin'])) {

                if (!isset($_SESSION['mailin'])) {

                    $_SESSION['mailin'] = new Mailin($config['mailin']['endpoint'], $config['mailin']['id'], $config['mailin']['secret']);
                }

                $_SESSION['mailin']->create_update_user(
                    $_SESSION['game']->getUser()->getEmail(),
                    array(
                        'EMAIL'  => $_SESSION['game']->getUser()->getEmail(),
                        'NAME'   => $_SESSION['game']->getUser()->getName(),
                        'LINK'   => $_SESSION['game']->getUser()->getLink(),
                        'GENDER' => $_SESSION['game']->getUser()->getGender(),
                        'LOCALE' => $_SESSION['game']->getUser()->getLocale(),
                    ),
                    0,
                    array(2),
                    array(),
                    0);
            }
        }
    }
}

if (!empty($_REQUEST['reset'])) $_SESSION['game']->resetGame();
if (!empty($_REQUEST['new'])) $_SESSION['game']->randomGame($db);
if (!empty($_REQUEST['id'])) $_SESSION['game']->userGame($db, $_REQUEST['id']);
if (empty($_REQUEST['cancel']) && !empty($_REQUEST['word']) && !empty($_REQUEST['hint'])) $_SESSION['game']->createGame($db, $_REQUEST);
if (empty($_REQUEST['cancel']) && !empty($_REQUEST['email']) && is_array($_REQUEST['email'])) {

    foreach (array_unique($_REQUEST['email']) as $email) {

        if (filter_var($email, FILTER_VALIDATE_EMAIL) && checkdnsrr(array_pop(explode('@', $email)), 'MX')) {

            if (!empty($config['mailin'])) {

                if (!isset($_SESSION['mailin'])) {

                    $_SESSION['mailin'] = new Mailin($config['mailin']['endpoint'], $config['mailin']['id'], $config['mailin']['secret']);
                }

                $to         = array($email => 'You'); //mandatory
                $subject    = "Can you solve this hangman puzzle?"; //mandatory
                $from       = array("no-reply@hangafriend.com", "Hang-A-Friend"); //mandatory
                $html       = require_once $path . '/template/email/share.html.phtml';; //mandatory
                $text       = require_once $path . '/template/email/share.txt.phtml';
                $cc         = array();
                $bcc        = array();
                $replyto    = array($_SESSION['game']->getUser()->getEmail(), $_SESSION['game']->getUser()->getName());
                $attachment = array();
                $headers    = array("Content-Type"=> "text/html; charset=iso-8859-1");

                $result = $_SESSION['mailin']->send_email($to, $subject, $from, $html, $text, $cc, $bcc, $replyto, $attachment, $headers);

                if (!empty($result) && $result['code'] === 'success') {

                    $_SESSION['game']->setMessage('Good news! Your emails were sent!');
                }
            }
        }
    }
}

if (!$_SESSION['game']->isError()) {

    $_SESSION['game']->playGame($_REQUEST);

    if ($_SESSION['game']->isOver()) {

        $_SESSION['game']->saveScore($db);
    }
}

if ($_SESSION['game']->getUser()->isConnected()) {

    if (!empty($_REQUEST['create'])) {

        $template = 'create';

    } elseif (!empty($_REQUEST['share'])) {

        $template = 'share';

    } else {

        $template = 'game';
    }

} else {

    $template = 'login';
}

$templatePath = $path . '/template/' . $template. '.phtml';

require_once $templatePath;
