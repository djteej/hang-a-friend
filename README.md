# Hang-A-Friend #

Hang-A-Friend is a super fun social hangman game where you can create hangman puzzles, share them with your friends, and solve puzzles created all around the World.


## System Requirements ##

Hang-A-Friend requires PHP 5.3 or later; we recommend using the latest PHP version whenever possible.


## Contributing ##

If you wish to contribute to Hang-A-Friend, please contact [Teej](mailto:djtrevorsmith@gmail.com)


## Creators ##

[Teej](http://teej.ca)


## Copyright and License ##

Copyright 2014 Teej. Released under the MIT license.